﻿using UnityEngine;
using System.Collections;

/*
 * All of the functions in here are and are used to make getting access
 * to input from the vive controllers less obnoxious
 */

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class ViveController : MonoBehaviour {
	public enum BUTTON { APPMENU, GRIP, TRIGGER, TOUCHPAD };
	public enum AXIS { TRIGGER, TOUCHPAD };

	private SteamVR_TrackedObject controller;
	public GameObject controllerGO;
	public Transform firePos; //TODO: move this to another script... 

	void Awake(){
		controller = GetComponent<SteamVR_TrackedObject> ();
	}

	#region ButtonInput
	/*
	 * AppMenu = SteamVR_Controller.ButtonMask.ApplicationMenu (button with 3 lines)
	 * Grip = SteamVR_Controller.ButtonMask.Grip
	 * Trigger = SteamVR_Controller.ButtonMask.Trigger
	 * Touchpad = SteamVR_Controller.ButtonMask.Touchpad
	 * Axis0 = Touchpad
	 * Axis1 = Trigger
	 * Axis2-4 = Nothing 
	 */ 

	public bool GetPress(BUTTON btn){
		if (controller.index == SteamVR_TrackedObject.EIndex.None){
			return false;
		}

		return SteamVR_Controller.Input ((int)controller.index).GetPress (ConvertToSteamVRBtn (btn));
	}

	public bool GetPressDown(BUTTON btn){
		if (controller.index == SteamVR_TrackedObject.EIndex.None){
			return false;
		}

		return SteamVR_Controller.Input ((int)controller.index).GetPressDown (ConvertToSteamVRBtn (btn));
	}

	public bool GetPressUp(BUTTON btn){
		if (controller.index == SteamVR_TrackedObject.EIndex.None){
			return false;
		}

		return SteamVR_Controller.Input ((int)controller.index).GetPressUp (ConvertToSteamVRBtn (btn));
	}
	#endregion ButtonInput

	#region AxisInput
	public Vector2 GetAxis(AXIS axis){
		if (controller.index == SteamVR_TrackedObject.EIndex.None){
			return Vector2.zero;
		}

		return SteamVR_Controller.Input ((int)controller.index).GetAxis(ConvertToSteamVRAxis(axis));
	}
	#endregion AxisInput

	#region Touchpad
	/*
	 * Touchpad Directions:
	 * ("front" of controller --> Tracking Circle + button w/ 3 lines this way)
	 * 			x=0,y=1
	 * x=-1,y=0			x=1, y=0
	 * 			x=0,y=-1
	 * ("back" of controller --> Vive Logo + menu button this way)
	 */ 
	public bool GetTouch(SteamVR_TrackedObject.EIndex controllerIndex){
		if (controller.index == SteamVR_TrackedObject.EIndex.None){
			return false;
		}

		return SteamVR_Controller.Input ((int)controller.index).GetTouch (SteamVR_Controller.ButtonMask.Touchpad);
	}

	public bool GetTouchDown(SteamVR_TrackedObject.EIndex controllerIndex){
		if (controller.index == SteamVR_TrackedObject.EIndex.None){
			return false;
		}

		return SteamVR_Controller.Input ((int)controller.index).GetTouchDown (SteamVR_Controller.ButtonMask.Touchpad);
	}
		
	public bool GetTouchUp(SteamVR_TrackedObject.EIndex controllerIndex){
		if (controller.index == SteamVR_TrackedObject.EIndex.None){
			return false;
		}

		return SteamVR_Controller.Input ((int)controller.index).GetTouchUp (SteamVR_Controller.ButtonMask.Touchpad);
	}
	#endregion Touchpad

	#region HairTrigger
	public bool GetHairTrigger(SteamVR_TrackedObject.EIndex controllerIndex){
		if (controller.index == SteamVR_TrackedObject.EIndex.None){
			return false;
		}

		return SteamVR_Controller.Input ((int)controller.index).GetHairTrigger ();
	}

	public bool GetHairTriggerDown(SteamVR_TrackedObject.EIndex controllerIndex){
		if (controller.index == SteamVR_TrackedObject.EIndex.None){
			return false;
		}

		return SteamVR_Controller.Input ((int)controller.index).GetHairTriggerDown ();
	}

	public bool GetHairTriggerUp(SteamVR_TrackedObject.EIndex controllerIndex){
		if (controller.index == SteamVR_TrackedObject.EIndex.None){
			return false;
		}

		return SteamVR_Controller.Input ((int)controller.index).GetHairTriggerUp ();
	}
	#endregion HairTrigger

	#region OtherData
	public Vector3 GetVelocity(SteamVR_TrackedObject.EIndex controllerIndex){
		if (controller.index == SteamVR_TrackedObject.EIndex.None){
			return Vector3.zero;
		}

		return SteamVR_Controller.Input ((int)controller.index).velocity;	
	}

	public Vector3 GetAngularVelocity(SteamVR_TrackedObject.EIndex controllerIndex){
		if (controller.index == SteamVR_TrackedObject.EIndex.None){
			return Vector3.zero;
		}
		return SteamVR_Controller.Input ((int)controller.index).angularVelocity;
	}

	public void DoRumble(ushort durationMicroSec = 500, Valve.VR.EVRButtonId buttonId = Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad){
		if (controller.index == SteamVR_TrackedObject.EIndex.None){
			return;
		}

		SteamVR_Controller.Input ((int)controller.index).TriggerHapticPulse (durationMicroSec, buttonId);
	}

	public SteamVR_Utils.RigidTransform GetRigidTransform(SteamVR_TrackedObject.EIndex controllerIndex){
		if (controller.index == SteamVR_TrackedObject.EIndex.None){
			return default(SteamVR_Utils.RigidTransform);
		}
		return SteamVR_Controller.Input ((int)controller.index).transform;
	}
	#endregion OtherData

	ulong ConvertToSteamVRBtn(BUTTON btn){
		switch(btn){
			case BUTTON.APPMENU: return SteamVR_Controller.ButtonMask.ApplicationMenu;
			case BUTTON.GRIP: return SteamVR_Controller.ButtonMask.Grip;
			case BUTTON.TRIGGER: return SteamVR_Controller.ButtonMask.Trigger;
			case BUTTON.TOUCHPAD: return SteamVR_Controller.ButtonMask.Touchpad;
			default: return SteamVR_Controller.ButtonMask.System;
		}
	}
	Valve.VR.EVRButtonId ConvertToSteamVRAxis(AXIS axis){
		switch (axis){
			case AXIS.TRIGGER: return Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
			case AXIS.TOUCHPAD: return Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad;
			default: return Valve.VR.EVRButtonId.k_EButton_System;
		}
	}
}
