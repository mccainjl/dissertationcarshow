﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{

    public string partID { get; set; }
    public InputField iPartID;

    public string Gender;
    public Dropdown gender;

    public ExperimentManager.Condition Condition;
    public Dropdown cond;


    public void StartButtonPressed()
    {
        if (iPartID.text == "")
        {
            Debug.Log("Please enter a participant ID before hitting start.");
            return;
        }

        partID = iPartID.text;

        if (gender.value == 0)
        {
            Gender = "Male";
        }
        else
        {
            Gender = "Female";
        }

        Condition = (ExperimentManager.Condition)cond.value;
    }

}