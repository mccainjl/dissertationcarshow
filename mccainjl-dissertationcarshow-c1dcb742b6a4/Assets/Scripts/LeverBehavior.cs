﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverBehavior : MonoBehaviour {
	public CarSelector carSelector;
    public bool leveron = false;
	public bool ranonce = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


        }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.tag + " " + other.name);
        if (other.gameObject.CompareTag("On"))
        {
            leveron = true;
            Debug.Log("On");
			if (carSelector != null)
			{ while (ranonce == false){
					carSelector.doRotate();}
            }
        }
        else if (other.gameObject.CompareTag("Off"))
        {
            leveron = false;
            Debug.Log("Off");
			if (carSelector != null)
			{ while (ranonce == false){
					carSelector.doReverse();}
            }
        }
        
    }
}
