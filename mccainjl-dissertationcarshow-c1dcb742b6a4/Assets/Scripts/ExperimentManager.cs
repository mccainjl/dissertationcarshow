﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;  

public class ExperimentManager : MonoBehaviour {
    public enum Condition { HIGH, LOW, NO };
    public Condition currentCondition;

    public enum ExperimentPhase { SETUP, TASK_ONE, EXIT, ERROR };
    public ExperimentPhase currentPhase;

    public delegate void ExperimentPhaseChanged(ExperimentPhase prevPhase, ExperimentPhase newPhase);
    public event ExperimentPhaseChanged OnExperimentPhaseChange;

    public JSONDataLogger dataLogger;
    //public MemoryTest memoryTest;

    //public Canvas canvas;
    //public GameObject CarStage;
    public List<GameObject> mainControllerModels = new List<GameObject>(); //add these in editor
    //public List<QuizQuestion> questions = new List<QuizQuestion>();

    [Header("Participant Settings")]
    public Canvas MenuCanvas;
    public InputField participantIDInputField;
    public int Gender;
    public Dropdown genderDropdown;
    public Dropdown conditionDropdown;

    public bool IsSetupComplete = false;
	public bool selectionmade = false;
    public bool IsTaskOneComplete = false;
    //public bool IsTaskTwoComplete = false;
    public bool finishExit = false;

    [Header("High Power Settings")]
    public List<GoGo> GoGoControllers = new List<GoGo>(); //add these in editor

    //[Header("Low Power settings")]
    //public List<Low> LowControllers = new List<Low>();

    [Header("Low Power Settings")]
	public List<VRGripper> VRGripper = new List<VRGripper>();
	public GameObject VRlever1; 
	public GameObject VRlever2;
	//public List<VRLever> VRlevers = new List<VRLever>();
    //public List<GameObject> Levers = new List<GameObject>();

    [Header("No Power Settings")]
    public CarSelector Carselector;
    
    // Use this for initialization
    void Start() {
        MenuCanvas.gameObject.SetActive(true);
        //canvas.gameObject.SetActive(false);
        IsSetupComplete = false;
        IsTaskOneComplete = false;
       // CarStage.SetActive(false);
       // IsTaskTwoComplete = false;
        finishExit = false;

        StartCoroutine(mainExperimentLoop());
    }

    // Update is called once per frame
    void Update() {

    }

    void UpdateExperimentPhase(ExperimentPhase newPhase)
    {
        if (OnExperimentPhaseChange != null)
        {
            OnExperimentPhaseChange(currentPhase, newPhase);
        }

        currentPhase = newPhase;
    }

    public void UpdateCondition(Condition newCondition) //sets up each condition with proper objects
    {
        currentCondition = newCondition;
        for (int i = 0; i < mainControllerModels.Count; i++)
        {
            mainControllerModels[i].SetActive(newCondition != Condition.HIGH);
			//VRGripper[i].enabled = (newCondition != Condition.HIGH);
            GoGoControllers[i].enabled = (newCondition == Condition.HIGH);
            GoGoControllers[i].gameObject.SetActive(newCondition == Condition.HIGH);
            
            //VRlevers[i].enabled = (newCondition == Condition.LOW);
            //Levers[i].SetActive(newCondition == Condition.LOW);
            //mainControllerModels[i].SetActive(newCondition == Condition.NO);
        }
        if (newCondition == Condition.NO)
        {
            Carselector.flooron = true;
        } 
		if (newCondition == Condition.LOW){
			VRlever1.SetActive(true);
			VRlever2.SetActive(true);
		}
    }
    IEnumerator mainExperimentLoop()
    {
        while (true)
        {
            yield return StartCoroutine(doSetup());
            yield return StartCoroutine(doTaskOne());
           // yield return StartCoroutine(doTaskTwo());
            yield return StartCoroutine(doExit());
        }
    }

    #region Setup
    IEnumerator doSetup()
    {
        //Does not have the previous phase check the others do since this is the first one in the loop
        UpdateExperimentPhase(ExperimentPhase.SETUP);

        IsTaskOneComplete = false;
        //IsTaskTwoComplete = false;
        finishExit = false;
        //canvas.gameObject.SetActive(false);
        //CarStage.gameObject.SetActive(true);

        while (!IsSetupComplete)
        {
            yield return null;
        }

        MenuCanvas.gameObject.SetActive(false);
        yield return null;
    }

    public void StartButtonPressed()
    {
		if (participantIDInputField.text == "")
        {
            Debug.Log("Please enter a participant ID before hitting start.");
            return;
        }

        currentCondition = (ExperimentManager.Condition)conditionDropdown.value;
        UpdateCondition(currentCondition);

        dataLogger.UpdateSetupData(participantIDInputField.text, currentCondition, genderDropdown.value);

        // DataLog.WriteLog("Participant ID: " + participantIDInputField.text +
        // "\r\nCondition: " + currentCondition.ToString() +
        //"\r\nGender: " + (genderDropdown.value == 0 ? "Male" : "Female") + "\r\n");  Remember to add Datalogger 
        IsSetupComplete = true;
    }
    #endregion Setup
    #region Task1
    IEnumerator doTaskOne()
    {
        if (currentPhase != ExperimentPhase.SETUP)
        {
            Debug.Log("Entered TASK_ONE phase from non-SETUP phase; exiting...");
            currentPhase = ExperimentPhase.ERROR;
            yield break;
        }

        UpdateExperimentPhase(ExperimentPhase.TASK_ONE);
        while (!IsTaskOneComplete || !Input.GetKey(KeyCode.Space))
        {
			selectionmade=false;
			//foreach (TargetHit th in TaskOneItems)
            //{
            //    IsTaskOneComplete = th.ImInBox != -1; Add new completion condition
            //}
            yield return null;
        }

        LogTaskOneData();  // customize each of these
        //OriginalLogTaskOneData();
        //TaskOneCleanup();
    }
    public void LogTaskOneData() {
        }
    //public void OriginalLogTaskOneData() { }
    //void TaskOneCleanup() { }

    #endregion Task1

   /* #region Task2
    IEnumerator doTaskTwo()
    {
        if (currentPhase != ExperimentPhase.TASK_ONE)
        {
            Debug.Log("Entered TASK_ONE phase from non-TASK_ONE phase; exiting...");
            currentPhase = ExperimentPhase.ERROR;
            yield break;
        }
        TaskTwoSetup();
        UpdateExperimentPhase(ExperimentPhase.TASK_TWO);

        //Do something for TaskTwo, perhaps wait for some keyboard input like hitting the space bar :D
        while (!IsTaskTwoComplete)
        {// || !Input.GetKey(KeyCode.Space)){
            yield return null;
        }

        dataLogger.UpdateTask2Data(questions);

            }
    void TaskTwoSetup()
    {
        canvas.gameObject.SetActive(true);
        //Spawn first set of items and first question
        memoryTest.Setup(questions); //Fix?
    }
    #endregion Task2*/
    IEnumerator doExit()
    {
        if (currentPhase != ExperimentPhase.TASK_ONE)
        {
            Debug.Log("Entered EXIT phase from non-TASK_ONE phase; exiting...");
            yield return StartCoroutine(doError());
            /*
				 * Given this structure, if a non-valid phase is entered at any point in the gameloop,
				 * it will terminate here, starting the error routine and saving the phase at which the error occurred
				 * so that the error coroutine can report it.
				 * 
				 * By making ONLY exit handle the error, we ensure that the other phases do not execute if a prior one had an error
				 */
        }

        UpdateExperimentPhase(ExperimentPhase.EXIT);
        dataLogger.DoLogData();
        DataLog.WriteLog("=========END TRIAL=========");

        //Do something for exit, perhaps wait for some keyboard input like hitting the space bar :D
        while (!finishExit || !Input.GetKey(KeyCode.Space))
        {
            yield return null;
        }
    }

    //Handles any error resulting from the main game loop
    IEnumerator doError()
    {
        Debug.Log("Entering ERROR phase from " + currentPhase.ToString() + " phase");
        UpdateExperimentPhase(ExperimentPhase.ERROR);

        //Do something for error, such as just waiting for a bit to display the error
        yield return new WaitForSeconds(1.5f);
    }
}


//add all list and selected car list and deleted list
//current car index refers to position