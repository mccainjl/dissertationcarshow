﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TargetHit : MonoBehaviour {
	public enum ItemType { SPORTS, ECONOMY };
	public ItemType myType;
	public GameObject headRig;
	public bool infront; 
	private Vector3 itemLoc;

	//for metric logging
	public bool pickedUp;
	public float timeHeld;
	public float proximityToHead;
	private float roundTime;
    public bool selected;

    //public string CarData;
	// Use this for initialization
	void Start () {
		Setup ();
	}

	public void Setup(){
		pickedUp = false;
		timeHeld = 0.0f;
		roundTime = 0.0f;
        selected = false;
        infront = false;

		itemLoc = transform.position;
		proximityToHead = 10000.0f;
	}
	
	// Update is called once per frame
	void Update () {
		float dist = Vector3.Distance(headRig.transform.position, transform.position);
		if (dist < proximityToHead) {
			proximityToHead = dist;
			//print("Closest Distance for " + transform.name + " is: " + dist);
		}

		roundTime += Time.deltaTime;
	}

	public string logString(){
		string myString = "Item name: " + transform.name + "\r\n";
		myString += "Picked up: " + pickedUp.ToString() + "\r\n";
		myString += "Time held: " + timeHeld.ToString() + "\r\n";
		myString += "Closest proximity to head: " + proximityToHead.ToString() + "\r\n";
		myString += "Round time: " + roundTime.ToString() + "\r\n";
        myString += "Selected: " + selected.ToString() + "\r\n";
		return myString;

	}
	/*void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag ("Box1")) {
			ImInBox = 1;
		} else if (other.gameObject.CompareTag ("Box2")) {
			ImInBox = 2;
		} else if (other.gameObject.CompareTag ("Box3")) {
			ImInBox = 3;
		}
	}*/

	/*void OnCollisionEnter(Collision c){
		//reinstantiate at instantiation location if dropped on floor
		//if (c.collider.gameObject.tag =="Floor") {
			//if we don't set it back to kinematic it will not stay in the air
			//Debug.Log ("hit floor");
			//GetComponent<Rigidbody> ().isKinematic = true;
			//reset the position
			//transform.position = itemLoc;
			//reset the rotation
			//transform.rotation = Quaternion.identity;
		//}
        if (c.gameObject.tag == "Front")
        {
            Debug.Log("Got Collision With" + c.gameObject.tag);
            infront = true;
        }
        else if (c.gameObject.tag == "Not")
        {
            Debug.Log("boolwassetfalse " + this.gameObject.name);
            infront = false;
        }
        else
        {
            Debug.Log("no colliders"+this.gameObject.name);
        }
        //Debug.Log("shit happened"+ c.gameObject.tag);
        if (infront == true)
        {
            Debug.Log("infronton"+this.gameObject.name);
        }
        else
        {
            Debug.Log("infrontoff"+this.gameObject.name);
        }
	}*/

	void OnTriggerStay (Collider c){
        if (c.gameObject.tag == "Front")
        {
            //Debug.Log("Got Collision With" + c.gameObject.tag);
            infront = true;
        }
       /* else if (c.gameObject.tag == "Not")
        {
            //Debug.Log("boolwassetfalse " + this.gameObject.name);
            infront = false;
        }
        else
        {
            Debug.Log("no colliders" + this.gameObject.name);
        }
        //Debug.Log("shit happened"+ c.gameObject.tag);
       /* if (infront == true)
        {
            Debug.Log("infronton" + this.gameObject.name);
        }
        else
        {
            Debug.Log("infrontoff" + this.gameObject.name);
        }*/
    }

    void OnTriggerExit (Collider c)
    {
        if (c.gameObject.tag == "Front")
        {
            infront = false;
        }
    }

	[System.Serializable]public class TargetHitData{
		public string name;
		public TargetHit.ItemType itemType;
		public bool pickedUp;
		public float timeHeld;
		public float closestToHead;
        public bool selected;

		public TargetHitData(string n = "", TargetHit.ItemType it = TargetHit.ItemType.SPORTS, 
			bool pick = false, float held = 0f, float close = 0f, bool chosen = false){
			name = n;
			itemType = it;
			pickedUp = pick;
			timeHeld = held;
			closestToHead = close;
            selected = chosen;
		}
        
	}

   /* IEnumerator carknows()
    {
        while (true)
        {
            infront = true;
            yield return null;
        }

    }*/
}

