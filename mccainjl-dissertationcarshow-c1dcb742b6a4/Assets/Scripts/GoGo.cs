﻿using UnityEngine;
using System.Collections;


public class GoGo : MonoBehaviour
{
    public Rigidbody controllerModelRB;
    public MeshRenderer controllerRenderer;
    public Transform reference;
    public float gogoScale = 1;
    public Transform headSpace;
    public float gogothreshold = .1f;

    public SteamVR_TrackedObject controller;
    public Transform heldobject = null;

    // Use this for initialization
    void Start()
    {
        controllerModelRB.maxAngularVelocity = Mathf.Infinity;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void LateUpdate()
    {

        Vector3 controllerPos_Head = headSpace.worldToLocalMatrix.MultiplyPoint(reference.position); //scaling world space to head space

        float offset_Head = controllerPos_Head.magnitude;

        Vector3 targetPosition = reference.position;

        if (offset_Head > gogothreshold)
        {
            Vector3 scalepoint = controllerPos_Head.normalized * gogothreshold; //k?
            Vector3 scaledPosition_Head = controllerPos_Head * gogoScale + scalepoint - gogoScale * scalepoint; //k - gk = b added on
            targetPosition = headSpace.localToWorldMatrix.MultiplyPoint(scaledPosition_Head); //scaling head space back to world space

        }

        Vector3 offset = targetPosition - transform.position;
        float t = Time.deltaTime;
        Vector3 velocity = offset / t;
        controllerModelRB.velocity = velocity;

        /*Quaternion offsetRotation = reference.rotation * Quaternion.Inverse (transform.rotation);
		Vector3 axis;
		float angle;
		offsetRotation.ToAngleAxis (out angle, out axis); 
		if (angle > 180) {
			angle = 360 - angle;
			axis = -axis;
		} else if (angle < -180) {
			angle = angle + 360;
		}
		t = Mathf.Max (t, 0.00001f);
		controllerModelRB.angularVelocity = angle * Mathf.Deg2Rad * axis / t;
		*/
        transform.rotation = reference.rotation;

        controllerRenderer.enabled = heldobject == null;

    }

    void OnTriggerStay(Collider C)
    {
        if (controller.index == SteamVR_TrackedObject.EIndex.None)
        {
            return;
        }
        //Debug.Log ("Got Collision With" + C.name);
        if (C.tag == "Item" && heldobject == null && enabled)
        {
            if (SteamVR_Controller.Input((int)controller.index).GetPress(SteamVR_Controller.ButtonMask.Trigger))
            {
                //Debug.Log (controller.index + "got trigger press ");
                Transform t = C.transform;
                t.parent = transform;

                t.GetComponent<Rigidbody>().isKinematic = true;
                heldobject = t;
                //let the item know it was picked up for metric logging
                heldobject.GetComponent<TargetHit>().pickedUp = true;
                StartCoroutine(waitForRelease());

            }
        }
    }
    /*public void HapticVibration(float _strength, float _duration)
{
    StartCoroutine(LongVibration(_strength, _duration));
}*/


    IEnumerator waitForRelease()
    {
        while (true)
        {
            //calculate time held for metric logging
            heldobject.GetComponent<TargetHit>().timeHeld += Time.deltaTime;
            if (!SteamVR_Controller.Input((int)controller.index).GetPress(SteamVR_Controller.ButtonMask.Trigger))
            {
                if (heldobject != null)
                {
                    heldobject.parent = null;
                    Rigidbody rb = heldobject.GetComponent<Rigidbody>();
                    if (rb != null)
                    {
                        rb.isKinematic = false;
                        rb.useGravity = true;
                        Vector3 Velocity = SteamVR_Controller.Input((int)controller.index).velocity;
                        rb.velocity = Velocity;
                    }
                    heldobject = null;
                }
                yield break;

            }
            yield return null;
        }
    }

}

   /* IEnumerator LongVibration(float _strength, float _duration)
    {

    // TODO: set vibration modes
    // Additive
    // Replace
    // Ignore

    if (isVibrating == true) // Already active so exit (will be updated to allow additive or raplce option in future versions
        yield break;

        isVibrating = true;

        _strength = Mathf.Clamp(_strength, 0, 1);
        if (controller == null || controller.index == SteamVR_TrackedObject.EIndex.None)
        {
            yield break;
        }
        var device = SteamVR_Controller.Input((int)controller.index); // Get the device 

    // The controller pulses for upto 3.999ms out of each 11ms window (90 fps). Which gives us our intensity.
    for (float i = 0; i < _duration; i += Time.fixedDeltaTime)
        {
            device.TriggerHapticPulse((ushort)(_strength * 3999), Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
            yield return null;
        }

    // finished
    isVibrating = false;
    }*/

