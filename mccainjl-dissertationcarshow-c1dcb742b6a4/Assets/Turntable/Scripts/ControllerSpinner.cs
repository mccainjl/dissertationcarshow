﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO: make sure adding the other controller doesn't mess things up...
//TODO: add two of these to the spinning platform, one for each controller
public class ControllerSpinner : MonoBehaviour {
	[SerializeField]
	private float verticalDistanceThreshold = 100;
	private bool attached = false;
	private float angleOffset;
	[SerializeField]
	private ViveController controller;
	//private float lastVel;
	private float[,] lastAngles = new float[2, 2];
	private int lastAnglesLength = 2;
	private int lastAnglesIndex;

	[SerializeField]
	private SpinningPlatformTag[] platforms;
	private Transform currentPlatform;
	private Transform lastPlatform;

	private float hue;
	[SerializeField]
	private bool hueShift = false;

	[SerializeField]
	private MeshRenderer rotaterRenderer;

	private bool smallSpinnerBeingGrabbed = false;

	// Use this for initialization
	void Start(){
		//GetComponent<Rigidbody>().centerOfMass = Vector3.zero;
		//GetComponent<Rigidbody>().inertiaTensor = Vector3.one;
	}

	// Update is called once per frame
	void Update(){
		if (!gameObject.activeInHierarchy) {
			return;
		}



		Debug.Log (smallSpinnerBeingGrabbed);
		if (controller.GetPress(ViveController.BUTTON.TOUCHPAD) && controller.transform.position.y-transform.position.y < verticalDistanceThreshold && currentPlatform!=null) {

			if (!attached){
				float origAngle = currentPlatform.transform.localEulerAngles.y;
				currentPlatform.transform.LookAt(controller.transform);
				currentPlatform.transform.localEulerAngles = new Vector3(0, currentPlatform.transform.localEulerAngles.y, 0);
				float lookAtAngle = currentPlatform.transform.localEulerAngles.y;
				angleOffset = origAngle - lookAtAngle;
				//angleOffset = Vector3.Angle(rightController.transform.position - transform.position, transform.forward);
			}
			attached = true;
		} else if (lastPlatform != null && lastPlatform != currentPlatform) {
			if (attached){

				lastPlatform.GetComponent<Rigidbody>().angularVelocity = new Vector3(
					0, 
					(lastAngles[(lastAnglesIndex+1) % lastAnglesLength, 0] - lastAngles[(lastAnglesIndex) % lastAnglesLength, 0]) * Mathf.PI / 180 / lastAngles[lastAnglesIndex % lastAnglesLength, 1],
					0
				);
			}
			attached = false;
		}

		if (attached) {
			currentPlatform.transform.LookAt(controller.transform);
			currentPlatform.transform.localEulerAngles = new Vector3(0, currentPlatform.transform.localEulerAngles.y + angleOffset, 0);
			lastAngles[lastAnglesIndex % lastAnglesLength, 0] = currentPlatform.transform.localEulerAngles.y;
			lastAngles[lastAnglesIndex % lastAnglesLength, 1] = Time.deltaTime;
			lastAnglesIndex++;
		}




		if (controller.GetPressDown (ViveController.BUTTON.TOUCHPAD)) {
			lastPlatform = currentPlatform;
			currentPlatform = null;
			foreach (SpinningPlatformTag s in platforms) {
				Transform t = s.transform;
				if (t.GetComponent<SpinningPlatformTag> ().isMainPlatform) {
					if (Vector3.Distance (transform.position, t.position) < 12f && currentPlatform == null) {
						currentPlatform = t;
					}
				} else {
					if (Vector3.Distance (transform.position, t.position) < 7.5f / 2.0f) {
						currentPlatform = t;
					}
				}
			}
		} else if (controller.GetPressUp (ViveController.BUTTON.TOUCHPAD)) {
			lastPlatform = currentPlatform;
			currentPlatform = null;
		}

	}
}
