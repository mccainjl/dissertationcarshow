﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;  // Used for getting the date
using System.Collections.Generic;

public class DataLog : MonoBehaviour {
	public static string path="./Metrics.txt";

	// Use this for initialization
	void Start () {
		if (File.Exists(path)){
			
		}
		//setup log file
		WriteLog("\r\n");
		WriteLog("=========NEW TRIAL=========");
		WriteLog(DateTime.Now.ToString());
		//end setup logfile
	}

	public static void WriteLog (string msg) {
		using (StreamWriter sw = File.AppendText(path)) 
		{
			sw.WriteLine(msg);
			sw.Close();
		}	
	}

	public static void WriteLog (List<string> msg) {
		using (StreamWriter sw = File.AppendText(path)) 
		{
			for (int i = 0; i < msg.Count; i++) {
				sw.WriteLine (msg);
			}
			sw.Close();
		}	
	}

	public static void TimeStamp(){

		WriteLog("Round Ended at: " + DateTime.Now.ToString() + "\r\n");
	}

	public static void WriteConsole(string msg){
		Debug.Log("<color=red>"+DateTime.Now+": "+msg+"</color>");
	}
}


