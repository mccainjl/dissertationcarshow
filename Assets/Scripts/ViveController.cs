﻿using UnityEngine;
using System.Collections;
/*
	Place me on both vive controllers
	
	All of the functions in here are static and are used to make getting access
	to input from the vive controllers less obnoxious.
*/
public class ViveController : MonoBehaviour {
	public SteamVR_TrackedObject controller;
	public Transform FirePos; //Child of controller in hierarchy, the position/rotation of where to fire rays from 

	#region Wrapper
	public enum BTN { LINE, GRIP, TRIGGER, TOUCHPAD };
	public enum AXIS { TRIGGER, TOUCHPAD };
	#region ButtonInput

	/*
	 * 3Line = SteamVR_Controller.ButtonMask.ApplicationMenu
	 * Grip = SteamVR_Controller.ButtonMask.Grip
	 * Trigger = SteamVR_Controller.ButtonMask.Trigger
	 * Touchpad = SteamVR_Controller.ButtonMask.Touchpad
	 */ 

	public static bool GetPress(SteamVR_TrackedObject.EIndex controllerIndex, BTN btn){
		return SteamVR_Controller.Input ((int)controllerIndex).GetPress (ConvertToSteamVRBtn (btn));
	}
	public static bool GetPressDown(SteamVR_TrackedObject.EIndex controllerIndex, BTN btn){
		return SteamVR_Controller.Input ((int)controllerIndex).GetPressDown (ConvertToSteamVRBtn (btn));
	}
	public static bool GetPressUp(SteamVR_TrackedObject.EIndex controllerIndex, BTN btn){
		return SteamVR_Controller.Input ((int)controllerIndex).GetPressUp (ConvertToSteamVRBtn (btn));
	}
	#endregion ButtonInput

	#region AxisInput
	public static Vector2 GetAxis(SteamVR_TrackedObject.EIndex controllerIndex, AXIS axis){
		return SteamVR_Controller.Input ((int)controllerIndex).GetAxis(ConvertToSteamVRAxis(axis));
	}
	#endregion AxisInput

	#region Touchpad
	/*
	 * Touchpad Directions:
	 * ("front" of controller --> Tracking Circle + button w/ 3 lines this way)
	 * 			x=0,y=1
	 * x=-1,y=0			x=1, y=0
	 * 			x=0,y=-1
	 * ("back" of controller --> Vive Logo + menu button this way)
	 */ 
	public static bool GetTouch(SteamVR_TrackedObject.EIndex controllerIndex){
		return SteamVR_Controller.Input ((int)controllerIndex).GetTouch (SteamVR_Controller.ButtonMask.Touchpad);
	}
	public static bool GetTouchDown(SteamVR_TrackedObject.EIndex controllerIndex){
		return SteamVR_Controller.Input ((int)controllerIndex).GetTouchDown (SteamVR_Controller.ButtonMask.Touchpad);
	}
	public static bool GetTouchUp(SteamVR_TrackedObject.EIndex controllerIndex){
		return SteamVR_Controller.Input ((int)controllerIndex).GetTouchUp (SteamVR_Controller.ButtonMask.Touchpad);
	}
	#endregion Touchpad

	#region OtherData
	public static Vector3 GetVelocity(SteamVR_TrackedObject.EIndex controllerIndex){
		return SteamVR_Controller.Input ((int)controllerIndex).velocity;	
	}

	public static Vector3 GetAngularVelocity(SteamVR_TrackedObject.EIndex controllerIndex){
		return SteamVR_Controller.Input ((int)controllerIndex).angularVelocity;
	}

	public static void DoRumble(SteamVR_TrackedObject.EIndex controllerIndex, ushort durationMicroSec = 500, Valve.VR.EVRButtonId buttonId = Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad){
		SteamVR_Controller.Input ((int)controllerIndex).TriggerHapticPulse (durationMicroSec, buttonId);
	}
	#endregion OtherData

	static ulong ConvertToSteamVRBtn(BTN btn){
		switch(btn){
			case BTN.LINE: return SteamVR_Controller.ButtonMask.ApplicationMenu;
			case BTN.GRIP: return SteamVR_Controller.ButtonMask.Grip;
			case BTN.TRIGGER: return SteamVR_Controller.ButtonMask.Trigger;
			case BTN.TOUCHPAD: return SteamVR_Controller.ButtonMask.Touchpad;
			default: return SteamVR_Controller.ButtonMask.System;
		}
	}
	static Valve.VR.EVRButtonId ConvertToSteamVRAxis(AXIS axis){
		switch (axis){
			case AXIS.TRIGGER: return Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
			case AXIS.TOUCHPAD: return Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad;
			default: return Valve.VR.EVRButtonId.k_EButton_System;
		}
	}
	#endregion Wrapper
}
