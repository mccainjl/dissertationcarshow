﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorRotater : MonoBehaviour {
    public bool flooron = false;
    public int speed = 1;
    public LeverBehavior lever;

	// Use this for initialization
	void Start () {
       // lever = GameObject.FindObjectOfType<LeverBehavior>();
	}

    // Update is called once per frame
    public void doRotate()
    {
        transform.Rotate(0, 60, 0);
    }

    public void doReverse()
    {
        transform.Rotate(0, -60, 0);
    }
    void Update () {
        if (flooron)
        {
            transform.Rotate(Vector3.up * Time.deltaTime*speed);
        }
                
    }
}
