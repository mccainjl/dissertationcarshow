﻿using UnityEngine;
using System.Collections;

public class ControllerBehaviorLeft : MonoBehaviour {

	public SteamVR_TrackedObject controller;
	public GameObject controllerModel;
	public Transform heldobject = null;
	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		controllerModel.SetActive (heldobject == null);

	}

	void OnTriggerenter (Collider C){
		//Debug.Log ("Got Collision With" + C.name);
	}

	void OnTriggerStay (Collider C) {
		//Debug.Log ("Got Collision With" + C.name);
		if (C.tag == "Lever" && heldobject == null && enabled) {
			if (SteamVR_Controller.Input ((int)controller.index).GetPress (SteamVR_Controller.ButtonMask.Trigger)) {
				//Debug.Log (deviceIndex + "got trigger press ");
				Transform t = C.transform;
				t.parent = transform;
				t.GetComponent<Rigidbody> ().isKinematic = true;
				heldobject = t;	
				//let the item know it was picked up for metric logging
				heldobject.GetComponent<TargetHit>().pickedUp = true;
				StartCoroutine (waitForRelease ());

			}
		}
	}

	IEnumerator waitForRelease (){
		while (true) {
			//calculate time held for metric logging
			heldobject.GetComponent<TargetHit>().timeHeld += Time.deltaTime;
			if (!SteamVR_Controller.Input ((int)controller.index).GetPress (SteamVR_Controller.ButtonMask.Trigger)) {
				if (heldobject != null) {
					heldobject.parent = null;
					Rigidbody rb = heldobject.GetComponent<Rigidbody> ();
					if (rb != null) {
						rb.isKinematic = false;
						rb.useGravity = true;
						Vector3 Velocity = SteamVR_Controller.Input ((int)controller.index).velocity;
						rb.velocity = Velocity;
					}
					heldobject = null;
				}
				yield break;
			}
			yield return null;
		}
	}
}