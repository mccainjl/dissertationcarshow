﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class JSONTester : MonoBehaviour {
    // Use this for initialization
    void Start()
    {
        /*StreamWriter sw = new StreamWriter ("./JSON.txt", false);

		#region Task1Data
		List<string> itemStringsBox1 = new List<string> ();
		itemStringsBox1.Add(GenerateItemJSONString ("bag-noname (1)", TargetHit.ItemType.BAG, 1, true, 0.5757439f, 0.8078828f, true));

		List<string> itemStringsBox2 = new List<string> ();
		itemStringsBox2.Add(GenerateItemJSONString ("watch-cartier", TargetHit.ItemType.WATCH, 2, true, 0.7482176f, 0.533262f, true));
		itemStringsBox2.Add(GenerateItemJSONString ("bag-louisvuitton (2)", TargetHit.ItemType.BAG, 2, true, 0.7539423f, 0.4433053f, true));
		itemStringsBox2.Add(GenerateItemJSONString ("sun-noname (1)", TargetHit.ItemType.SUNGLASSES, 2, true, 0.7468367f, 0.5527363f, true));
		itemStringsBox2.Add(GenerateItemJSONString ("watch-noname (1)", TargetHit.ItemType.WATCH, 2, true, 1.359977f, 0.620792f, true));
		itemStringsBox2.Add(GenerateItemJSONString ("watch-timex (1)", TargetHit.ItemType.WATCH, 2, true, 0.9449887f, 0.5595741f, true));
		itemStringsBox2.Add(GenerateItemJSONString ("sun-prada (1)", TargetHit.ItemType.SUNGLASSES, 2, true, 1.038658f, 0.6333576f, true));
		itemStringsBox2.Add(GenerateItemJSONString ("sun-rayban (1)", TargetHit.ItemType.SUNGLASSES, 2, true, 0.7767485f, 0.6289772f, true));
		itemStringsBox2.Add(GenerateItemJSONString ("bag-coach (1)", TargetHit.ItemType.BAG, 2, true, 1.127803f, 0.4923113f, true));

		List<string> itemStringsBox3 = new List<string> ();

		List<string> boxString = new List<string> ();
		boxString.Add (GenerateBoxJSONString ("Box1", 1, itemStringsBox1, true));
		boxString.Add (GenerateBoxJSONString ("Box2", 2, itemStringsBox2, true));
		boxString.Add (GenerateBoxJSONString ("Box3", 3, itemStringsBox3, true));

		string task1DataStr = GenerateTaskOneDataJSONString ("task1Data", 44.56008f, GenerateBoxesListJSONString ("boxes", boxString, false), false);
		#endregion Task1Data

		#region Task2Data
		List<string> dummyTask2BagList = new List<string>();
		dummyTask2BagList.Add(GenerateItemJSONString("bag-louisvuitton(CLONE)", TargetHit.ItemType.BAG, -1, true, 0.7539423f, 0.4433053f, true));
		dummyTask2BagList.Add(GenerateItemJSONString ("bag-noname(CLONE)", TargetHit.ItemType.BAG, -1, true, 0.5757439f, 0.8078828f, true));
		dummyTask2BagList.Add(GenerateItemJSONString ("bag-coach(CLONE)", TargetHit.ItemType.BAG, -1, true, 1.127803f, 0.4923113f, true));
		List<string> dummyTask2SunglassesList = new List<string>();
		dummyTask2SunglassesList.Add(GenerateItemJSONString ("sun-noname(CLONE)", TargetHit.ItemType.SUNGLASSES, -1, true, 0.7468367f, 0.5527363f, true));
		dummyTask2SunglassesList.Add(GenerateItemJSONString ("sun-prada(CLONE)", TargetHit.ItemType.SUNGLASSES, -1, true, 1.038658f, 0.6333576f, true));
		dummyTask2SunglassesList.Add(GenerateItemJSONString ("sun-rayban(CLONE)", TargetHit.ItemType.SUNGLASSES, -1, true, 0.7767485f, 0.6289772f, true));
		List<string> dummyTask2WatchList = new List<string>();
		dummyTask2WatchList.Add(GenerateItemJSONString ("watch-cartier(CLONE)", TargetHit.ItemType.WATCH, -1, true, 0.7482176f, 0.533262f, true));
		dummyTask2WatchList.Add(GenerateItemJSONString ("watch-noname(CLONE)", TargetHit.ItemType.WATCH, -1, true, 1.359977f, 0.620792f, true));
		dummyTask2WatchList.Add(GenerateItemJSONString ("watch-timex(CLONE)", TargetHit.ItemType.WATCH, -1, true, 0.9449887f, 0.5595741f, true));

		List<string> questionsList = new List<string>();
		questionsList.Add(GenerateQuestionJSONString(1, 0.5f, "Which of these items was mostly grey?", "watch-cartier(CLONE)", true, TargetHit.ItemType.WATCH, dummyTask2WatchList, true));
		questionsList.Add(GenerateQuestionJSONString(2, 0.5f, "Which of these objects was mostly brown?", "bag-coach(CLONE)", true, TargetHit.ItemType.BAG, dummyTask2BagList, true));
		questionsList.Add(GenerateQuestionJSONString(3, 0.5f, "Which of these items is the most expensive brand?", "bag-louisvuitton(CLONE)", true, TargetHit.ItemType.BAG, dummyTask2BagList, true));
		questionsList.Add(GenerateQuestionJSONString(4, 0.5f, "Which of these objects was mostly black?", "watch-noname(CLONE)", true, TargetHit.ItemType.WATCH, dummyTask2WatchList, true));
		questionsList.Add(GenerateQuestionJSONString(5, 0.5f, "Which of these items was not a name brand?", "watch-noname(CLONE)", true, TargetHit.ItemType.WATCH, dummyTask2WatchList, true));

//		string task2DataStr = GenerateTaskTwoDataJSONString("task2Data", 0.5f, GenerateQuestionsListJSONString("questions", questionsList, false), false);
		#endregion Task2Data

//		string partStr = GenerateParticipantJSONString (System.DateTime.Now.ToString (), "3", ExperimentManager.Condition.CBLEFT, 0, task1DataStr, task2DataStr, true);

//		sw.WriteLine (partStr);
		sw.Close ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public string GenerateItemJSONString(string name, TargetHit.ItemType itemType, int boxNum, bool pickedUp, 
		float timeHeld, float closestDistToHead, bool withBrace = true){
		string itemStr = (withBrace ? "{" : ""); //Add brace if indicated
		itemStr += "\"name\":\"" + name + "\",";
		itemStr += "\"type\":\"" + (int)itemType + "\",";
		itemStr += "\"box\":\"" + boxNum + "\",";
		itemStr += "\"picked_up\":\"" + (pickedUp ? 1 : 0) + "\",";
		itemStr += "\"time_held\":\"" + timeHeld + "\",";
		itemStr += "\"closest_to_head\":\"" + closestDistToHead + "\"";
		itemStr += (withBrace ? "}" : ""); //Finally, close brace if indicated
		return itemStr;
	}

	public string GenerateBoxJSONString(string name, int id, List<string> items, bool withBrace = true){
		string boxStr = (withBrace ? "{" : "");
		boxStr += "\"name\":\"" + name + "\",";
		boxStr += "\"id\":\"" + id + "\",";
		boxStr += "\"contents\":[";

		//now add the item info
		for (int i = 0; i < items.Count; i++){
			boxStr += items [i];
			if (i < items.Count - 1){
				boxStr += ",";
			}
		}

		boxStr += "]";
		boxStr += (withBrace ? "}" : "");
		return boxStr;
	}

	public string GenerateBoxesListJSONString(string name, List<string> boxStrings, bool withBrace = true){
		string boxesList = (withBrace ? "{" : "");
		boxesList += "\"boxes\":[";

		for (int i = 0; i < boxStrings.Count; i++){
			boxesList += boxStrings [i];
			if (i < boxStrings.Count - 1){
				boxesList += ",";
			}
		}

		boxesList += "]";
		boxesList += (withBrace ? "}" : "");
		return boxesList;
	}

	public string GenerateTaskOneDataJSONString(string name, float timeForTask, string boxData, bool withBrace = true){
		string taskOneStr = (withBrace ? "{" : "");
		taskOneStr += "\"task1Data\":{\"task_time\":\"" + timeForTask + "\",";
		taskOneStr += boxData;
		taskOneStr += "}";
		taskOneStr += (withBrace ? "}" : "");
		return taskOneStr;
	}

	public string GenerateQuestionJSONString(int number, float timeSpent, string question, string answer, bool isCorrect, 
		TargetHit.ItemType itemType, List<string> items, bool withBrace = true){
		string questionStr = (withBrace ? "{" : "");
		questionStr += "\"number\":\"" + number + "\",";
		questionStr += "\"time_spent\":\"" + timeSpent + "\",";
		questionStr += "\"question\":\"" + question + "\",";
		questionStr += "\"answer\":\"" + answer + "\",";
		questionStr += "\"correct\":\"" + (isCorrect ? 1 : 0) + "\",";
		questionStr += "\"item_type\":\"" + (int) itemType + "\",";
		questionStr += "\"items\":[";

		//Now add the item info
		for (int i = 0; i < items.Count; i++){
			questionStr += items [i];
			if (i < items.Count - 1){
				questionStr += ",";
			}
		}

		questionStr += "]";
		questionStr += (withBrace ? "}" : "");
		return questionStr;
	}

	public string GenerateQuestionsListJSONString(string name, List<string> questionStrings, bool withBrace = true){
		string questionsList = (withBrace ? "{" : "");
		questionsList += "\"questions\":[";

		for (int i = 0; i < questionStrings.Count; i++){
			questionsList += questionStrings [i];
			if (i < questionStrings.Count - 1){
				questionsList += ",";
			}
		}

		questionsList += "]";
		questionsList += (withBrace ? "}" : "");
		return questionsList;
	}

	public string GenerateTaskTwoDataJSONString(string name, float timeForTask, string questionData, bool withBrace = true){
		string taskTwoStr = (withBrace ? "{" : "");
		taskTwoStr += "\"task2Data\":{\"task_time\":\"" + timeForTask + "\",";
		taskTwoStr += questionData;
		taskTwoStr += "}";
		taskTwoStr += (withBrace ? "}" : "");
		return taskTwoStr;
	}

	public string GenerateParticipantJSONString(string date, string participantID, ExperimentManager.Condition condition, 
		int gender, string task1Data, string task2Data, bool withBrace = true){
		string partStr = (withBrace ? "{" : "");
		partStr += "\"date\":\"" + date + "\",";
		partStr += "\"participant_id\":\"" + participantID + "\",";
		partStr += "\"condition\":\"" + (int)condition + "\",";
		partStr += "\"gender\":\"" + gender + "\",";
		partStr += task1Data + ",";
		partStr += task2Data;
		partStr += (withBrace ? "}" : "");
		return partStr;
	}
    */
    }
}

