﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class JSONDataLogger : MonoBehaviour
{
    public string DataDir = "./Data/";
    public string Path = "";

    [Header("Setup Settings")]
    public string DateStr = "";
    public string ParticipantID = "";
    public ExperimentManager.Condition Condition;
    public int Gender = 0;
    public float ExperimentStartTime = 0f;
    public float ExperimentEndTime = 0f;

    [Header("Task One Settings")]
    public float TaskOneStartTime = 0f;
    public float TaskOneEndTime = 0f;
    public List<TargetHit.TargetHitData> carsContent = new List<TargetHit.TargetHitData>();
    public List<CarSelector.deletedcars> deletedlist = new List<CarSelector.deletedcars>();

    void Start()
    {
        DateStr = System.DateTime.Now.ToString();

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DoLogData()
    {
        StreamWriter sw = new StreamWriter(Path, false);

        string Task1DataStr = GetTaskOneDataJSONString();

        string ParticipantStr = GenerateParticipantJSONString(DateStr, ParticipantID, Condition, Gender,
            ExperimentEndTime - ExperimentStartTime, Task1DataStr, true
        );

        string deletedCarsDataStr = GetdeletedcarsDataStr();

        Debug.Log(ParticipantStr);
        sw.WriteLine(ParticipantStr);
        sw.Close();
    }

    private string GenerateParticipantJSONString(string date, string participantID, ExperimentManager.Condition condition,
        int gender, float expTime, string task1Data, bool withBrace = true)
    {
        string partStr = (withBrace ? "{" : "");
        partStr += "\"date\":\"" + date + "\",";
        partStr += "\"participant_id\":\"" + participantID + "\",";
        partStr += "\"condition\":\"" + (int)condition + "\",";
        partStr += "\"gender\":\"" + gender + "\",";
        partStr += "\"time\":\"" + expTime + "\",";
        partStr += task1Data + ",";
        partStr += (withBrace ? "}" : "");
        return partStr;
    }

    #region SetupRelated
    public void UpdateSetupData(string partID, ExperimentManager.Condition condition, int gender)
    {
        ParticipantID = partID;
        Condition = condition;
        Gender = gender;
        ExperimentStartTime = Time.time;
        TaskOneStartTime = Time.time;

        Path = DataDir + ParticipantID + "-" + Condition.ToString() + "-data.txt";
    }
    #endregion SetupRelated

    #region Task1Related
    public void UpdateTask1Data(List<TargetHit.TargetHitData> cars)
    {
        TaskOneEndTime = Time.time;

        for (int i = 0; i < cars.Count; i++)
        {
            carsContent.Add(new TargetHit.TargetHitData(
                cars[i].name, cars[i].itemType,
                cars[i].pickedUp, cars[i].timeHeld, cars[i].closestToHead, cars[i].selected
            ));
        }


    }

    public string GetTaskOneDataJSONString()
    {
        List<string> Carstats = new List<string>();

        //Get the string from the contents of the box
        for (int i = 0; i < carsContent.Count; i++)
        {
            Carstats.Add(GenerateItemJSONString(
                carsContent[i].name,
                carsContent[i].itemType,
                carsContent[i].pickedUp,
                carsContent[i].timeHeld,
                carsContent[i].closestToHead,
                carsContent[i].selected,
                true
            ));
        }
        string carstatsstr = GeneratecarstatsJSONstring("carstats", Carstats, false);
        return GenerateTaskOneDataJSONString("task1Data", TaskOneEndTime - TaskOneStartTime, carstatsstr, false);
    }

    //Now generate the string of the box contents 
    //Cars.Add (GenerateBoxJSONString (Eliminated, true));





    private string GenerateTaskOneDataJSONString(string name, float timeForTask, string carData, bool withBrace = true)
    {
        string taskOneStr = (withBrace ? "{" : "");
        taskOneStr += "\"task1Data\":{\"task_time\":\"" + timeForTask + "\",";
        taskOneStr += carData;
        taskOneStr += "}";
        taskOneStr += (withBrace ? "}" : "");
        return taskOneStr;
    }

    private string GeneratecarstatsJSONstring(string name, List<string> carStrings, bool withBrace = true)
    {
        string carstats = (withBrace ? "{" : "");
        carstats += "\"cars\":[";
        for (int i = 0; i < carStrings.Count; i++)
        {
            carstats += carStrings[i];
            if (i < carStrings.Count - 1)
            {
                carstats += ",";
            }
        }

        carstats += "]";
        carstats += (withBrace ? "}" : "");
        return carstats;
    }

    private string GenerateItemJSONString(string name, TargetHit.ItemType itemType, bool pickedUp,
        float timeHeld, float closestDistToHead, bool selected = false, bool withBrace = true)
    {
        string itemStr = (withBrace ? "{" : ""); //Add brace if indicated
        itemStr += "\"name\":\"" + name + "\",";
        itemStr += "\"type\":\"" + (int)itemType + "\",";
        itemStr += "\"picked_up\":\"" + (pickedUp ? 1 : 0) + "\",";
        itemStr += "\"time_held\":\"" + timeHeld + "\",";
        itemStr += "\"closest_to_head\":\"" + closestDistToHead + "\"";
        itemStr += "\"Selected\":\"" + selected + "\"";
        itemStr += (withBrace ? "}" : ""); //Finally, close brace if indicated
        return itemStr;
    }
    #endregion Task1Related

    public void UpdateEliminatedData(List<CarSelector.deletedcars> carorder)
    {
        for (int i = 0; i < carorder.Count; i++)
        {
            deletedlist.Add(new CarSelector.deletedcars(
                carorder[i].Name, carorder[i].timeEliminated
            ));
        }

    }
    public string GetdeletedcarsDataStr()
    {
        List<string> Carsdel = new List<string>();
        for (int i = 0; i < deletedlist.Count; i++)
        {
            Carsdel.Add(GeneratecarsJSONString(
                deletedlist[i].Name,
                deletedlist[i].timeEliminated,
                true
            ));
        }
        string carstatsstr = GeneratecarstatsJSONstring("carstats", Carsdel, false);
        return GenerateEliminationJSONString("EliminatedList", carstatsstr, false);
    }
    private string GeneratecarsJSONString(string Name, float timeEliminated, bool withBrace = true)
    {
        string eliminatedStr = (withBrace ? "{" : "");
        eliminatedStr += "\"Name\":\"" + Name + "\",";
        eliminatedStr += "\"Time Eliminated\":\"" + timeEliminated;
        eliminatedStr += "}";
        eliminatedStr += (withBrace ? "}" : "");
        return eliminatedStr;
    }

    private string GenerateEliminationJSONString(string Name, string carstuff, bool withBrace = true)
    {
        string finishedliststr = (withBrace ? "{" : "");
        finishedliststr += "\"Name\":\"" + Name + "\",";
        finishedliststr += carstuff;
        finishedliststr += (withBrace ? "}" : "");
        return finishedliststr;
    }
}
