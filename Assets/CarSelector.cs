﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSelector : MonoBehaviour {

	public bool flooron = false;
	public int speed = 1;
	public LeverBehavior lever;
    public List<CarScript> mastercarlist = new List<CarScript>();
    public string Eliminated = "";
	public List<Transform> snaptopositions = new List<Transform>();
	public List<bool> occupied = new List<bool>();
	public List<CarScript> deleted = new List<CarScript>();
	public int currentCarIndex;
	public Transform selectoredge;
	public Transform selectorcenter;
	public ExperimentManager experimentManager;
	public LeverBehavior leverBehavior;
    public TargetHit TargetHit;

    //for data logging

    public string Name;
    public float timeEliminated;


    // Use this for initialization
    void Start () {

		initialcarpos();
	
	}
	
	// Update is called once per frame
	void Update () {
		if (flooron)
		{
			transform.Rotate(Vector3.up * Time.deltaTime*speed);
		}
	}

	public void initialcarpos () {
		float deg = mastercarlist.Count > 0 ? 360 / mastercarlist.Count : 0; //if mastercarlist count is greater than 0 ? do this: otherwise do that
		float radius = (selectoredge.position - selectorcenter.position).magnitude; 

		for (int i = 0; i < mastercarlist.Count; i++){
			mastercarlist[i].transform.position = snaptopositions[i].transform.position;
			//GetComponent<Rigidbody>(mastercarlist[i]).constraints = RigidbodyConstraints.FreezeAll;
		}
	}

	//public void updatecarpos(){
	//	for (int i = 0; i < mastercarlist.Count; i++){
	//		findcarpos(mastercarlist[i].transform);
	//		mastercarlist[i].transform.position=closestpos.position;
	//}
	//}


		

	public void eliminatecar() {
        Eliminated = Eliminated + System.DateTime.Now.ToString(); //this makes a string
        deleted.Add (mastercarlist[currentCarIndex]); //this makes a list
		mastercarlist[currentCarIndex].gameObject.SetActive(false);
        	}

	public void selectcar() {
		//add something here to record car
        if (mastercarlist[currentCarIndex].gameObject.activeInHierarchy) {
            //mastercarlist[currentCarIndex].gameObject.GetComponent<TargetHit>.selected = true;
            experimentManager.selectionmade=true;
            Debug.Log("Car Selected");
		}
        
		//else { //todo: pop up message saying "You have eliminated this car"
		//}
	}

	public void doRotate()
	{
		float deg = mastercarlist.Count > 0 ? 360 / mastercarlist.Count : 0;
		transform.Rotate(0, deg, 0);
		leverBehavior.ranonce =true;

		if (currentCarIndex == mastercarlist.Count - 1){
			currentCarIndex = 0;
		}else{
			currentCarIndex++;
		}
	}

	public void doReverse()
	{
		float deg = mastercarlist.Count > 0 ? 360 / mastercarlist.Count : 0;
		transform.Rotate(0, -deg, 0);
		leverBehavior.ranonce =true;

		if (currentCarIndex == 0){
			currentCarIndex = mastercarlist.Count - 1;
		}else{
			currentCarIndex--;//make sure it doesn't go to -1, loop it back around
		}
	}

    public class deletedcars
    {
        public string Name;
        public float timeEliminated;

        public deletedcars(string n = "", float time = 0f)
        {
            Name = n;
            timeEliminated = time;
        }
    } 

}
